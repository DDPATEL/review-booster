<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Auth::routes();

//Route::get('admin/login','admin\AuthController@index');

//Admin Route
Route::get('/', function () {

	if (!Auth::check()) {
		return view('admin.login');
	}
	else{
		return Redirect::to("admin");
	}
});

Route::post('admin/login', 'admin\AuthController@dologin');

Route::get('admin/logout', 'admin\AuthController@logout');


Route::group(['prefix' => 'admin'], function() {

	/* Dashboard */
	Route::get('dashboard', 'admin\HomeController@dashboard');

	/* User Module */
	Route::resource('users', 'admin\UserController');
	Route::post('users/store', 'admin\UserController@store');
	Route::get('users/edit/{id}', 'admin\UserController@edit');
	Route::post('users/update/{id}', 'admin\UserController@update');
	Route::get('users/checkpassword/{id}', 'admin\UserController@checkpassword');
	Route::post('users/changepassword/{id}', 'admin\UserController@changepassword');
	Route::get('users/destroy/{id}', 'admin\UserController@destroy');

 /* Role */

 Route::resource('role', 'admin\RoleController');
 Route::post('role/store', 'admin\RoleController@store');
 Route::get('role/edit/{id}', 'admin\RoleController@edit');
 Route::post('role/update/{id}', 'admin\RoleController@update');
 Route::get('role/destroy/{id}', 'admin\RoleController@destroy');

});


//Password reset routes
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
