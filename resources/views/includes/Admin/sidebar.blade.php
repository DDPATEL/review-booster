<div class="page-sidebar-wrapper">

                    <div class="page-sidebar navbar-collapse collapse">

                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">

                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->

                            <li class="nav-item active">
                                <a href="{{url::to('admin/home')}}" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                </a>

                            </li>




                             <li class="nav-item">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Users</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="{{url::to('admin/users')}}" class="nav-link ">
                                            <span class="title">View</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{url::to('admin/users/create')}}" class="nav-link ">
                                            <span class="title">Add</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item">
                               <a href="javascript:;" class="nav-link nav-toggle">
                                   <i class="icon-diamond"></i>
                                   <span class="title">Role</span>
                                   <span class="arrow"></span>
                               </a>
                               <ul class="sub-menu">
                                   <li class="nav-item">
                                       <a href="{{url::to('admin/role')}}" class="nav-link ">
                                           <span class="title">View</span>
                                       </a>
                                   </li>
                                   <li class="nav-item">
                                       <a href="{{url::to('admin/role/create')}}" class="nav-link ">
                                           <span class="title">Add</span>
                                       </a>
                                   </li>
                               </ul>
                           </li>




                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
