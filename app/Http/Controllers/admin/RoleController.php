<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Auth;
use DB;
use App\Role;


class RoleController extends Controller
{

  public function index()
  {
    $role = Role::get();

      return view('admin.role.index',compact('role'));
  }

  public function create()
  {

      return view('admin.role.create');
  }

  public function store(Request $request)
  {

    $rules = [
      'name' => 'required',
      'display_name' => 'required',
      'description' => 'required',

    ];

    $validator = Validator::make($request->all(),$rules);

    if($validator->fails()){

      return redirect()
           ->back()
           ->withErrors($validator)
           ->withInput();

    }
    // check role already exist START
        $checkRoleName = Role::where('name',$request->name)->first();

        if(!is_null($checkRoleName)){
          Session::flash('message', '<div class="alert alert-danger"><strong>Failed!</strong> Role Name Already Exist.!! </div>');
          return redirect('admin/role/create')->withInput();
        }
    // check role already exist END

    // check role display name already exist START
        $checkRoleDisplayName = Role::where('display_name',$request->display_name)->first();

        if(!is_null($checkRoleDisplayName)){
          Session::flash('message', '<div class="alert alert-danger"><strong>Failed!</strong> Role Display Name Already Exist.!! </div>');
          return redirect('admin/role/create')->withInput();
        }
    // check role display name already exist END
    $user = new Role();
    $data['name'] = $request->name;
    $data['display_name'] = $request->display_name;
    $data['description'] = $request->description;

    $user->fill($data);
    $user->save();

      Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Role Added Successfully.!! </div>');
      return redirect('admin/role');

  }

  public function edit(Request $request, $id)
  {
      $roleId = $request->id;
      $getRoleDetail = Role::where('id',$roleId)->first();

      return view('admin.role.edit',compact('getRoleDetail'));
  }

  public function update(Request $request, $id)
  {
      $roleId = $request->id;

      $rules = [
        'name' => 'required',
        'display_name' => 'required',
        'description' => 'required',
      ];

      $validator = Validator::make($request->all(),$rules);

      if($validator->fails()){

        return redirect()
             ->back()
             ->withErrors($validator)
             ->withInput();

      }
      // check role already exist START
          $checkRoleName = Role::where('name',$request->name)->first();

          if(!is_null($checkRoleName)){
            Session::flash('message', '<div class="alert alert-danger"><strong>Failed!</strong> Role Name Already Exist.!! </div>');
            return redirect('admin/role');
          }
      // check role already exist END
      
      // check role display name already exist START
          $checkRoleDisplayName = Role::where('display_name',$request->display_name)->first();

          if(!is_null($checkRoleDisplayName)){
            Session::flash('message', '<div class="alert alert-danger"><strong>Failed!</strong> Role Display Name Already Exist.!! </div>');
            return redirect('admin/role/create')->withInput();
          }
      // check role display name already exist END
      $data['name'] = $request->name;
      $data['display_name'] = $request->display_name;
      $data['description'] = $request->description;

      $updateRole = Role::where('id',$roleId)->update($data);

      Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Role Updated Successfully.!! </div>');
      return redirect('admin/role');
  }

  public function destroy(Request $request,$id)
  {
    $roleId = $request->id;

    $deleteUser = Role::where('id',$roleId)->delete();
    Session::flash('message', '<div class="alert alert-success"><strong>Success!</strong> Role Deleted Successfully.!! </div>');
    return redirect('admin/role');
  }
}
