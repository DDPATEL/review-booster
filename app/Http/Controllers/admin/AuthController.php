<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use Illuminate\Support\Facades\Redirect;



class AuthController extends Controller
{

 	public function index()
 	{
 		return view('admin.login');
 	}

 	public function dologin(Request $request)
 	{
 		$rules = [
                'email' =>'required|email',
                'password' => 'required'
        ];
        $validator = Validator::make($request->all(),$rules);

        if($validator->fails()){
        	return redirect()->back()
            ->withInput()
            ->withErrors($validator);

        }

        $logindetails = array(
        				'email' => $request['email'],
            			'password' => $request['password']);

        if(Auth::attempt($logindetails))
        {
        	if(Auth::user()->hasRole('admin'))
            {

                return Redirect::to('admin/dashboard');
            }
            Auth::logout();
            return redirect()->back()
                        ->withErrors(['email' =>"Only Admin Can Login Here..!"]);

        }
        else
        {
        	return redirect()->back()
						->withErrors(['email' =>'Invalid Login Details.']);
        }
 	}

 	public function logout()
 	{
 		Auth::logout();
    return view('admin.login');
 	}
}
